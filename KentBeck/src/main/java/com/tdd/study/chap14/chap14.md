#study_tdd
- 바꾸기
    - 이번에는 좀 단순한 변화에 대해 생각해볼 것이다. 2프랑이 있는데 이걸 달러로 바꾸고 싶다.
        ```java
              public void testReduceMoneyDifferentCurrency() {
                  Bank bank = new Bank();
                  bank.addRate("CHF","USD",2);
                  Money result = bank.reduce(Money.franc(2), "USD");
                  assertEquals(Money.dollar(1), result);
              }
        ```
        - 프랑을 달러로 변환할 때 나누기 2를 한다.
        ```java
              Money 
              public Money reduce(String to) {
                  int rate = (currency.equals("CHF") && to.equals("USD")) ? 2 : 1;
                  return new Money(amount / rate, to);
              }
        ```
        - 이 코드로 인해서 갑자기 Money 가 환율에 대해 알게 돼 버렸다. 환율에 대한 일은 모두 Bank 가 처리해야 한다.
        - Expression.reduce() 의 인자로 Bank 를 넘겨야 할것이다.
        ```java
              Bank
              Money reduce(Expression source, String to) {
                  return source.reduce(this, to);
              }
              
              Expression
              Money reduce(Bank bank, String to);

              Sum
              public Money reduce(Bank bank, String to) {
                  int amount = augend.amount + addend.amount;
                  return new Money(amount, to);
              }
        
              Money 
               public Money reduce(String to) {
                  int rate = (currency.equals("CHF") && to.equals("USD")) ? 2 : 1;
                  return new Money(amount / rate, to);
               }
        ```
        - 이젠 환율을 Bank 에서 계산할 수 있게 됐다.
        ```java
              Bank
              int rate(String from, String to) {
                  return (from.equals("CHF") && to.equals("USD")) ? 2 : 1;
              }
        ```
        - 올바른 환율을 bank 에게 물어보자
        ```java
              Money
              public Money reduce(Bank bank, String to) {
                  int rate = bank.rate(currency, to);
                  return new Money(amount/rate, to);
              }
        ```
        - 귀찮은 2가 아직도 테스트와 코드 두 부분에 모두 나온다. 이걸 없애버리려면 Bank 에서 환율표를 가지고 있다가 필요할때 찾아볼수 있게 해야한다.
        - 두개의 통화와 환율을 매핑시키는 해시 테이블을 사용할 수있겠다.
        - 통화 쌍을 해시 테이블의 키로 쓰기 위해 배열을 사용할 수 있을까??
        - Array.equals()가 각각의 원소에 대한 동치성 검사를 수행하는지 모르겠다.
        ```java
              public void testArrayEquals() {
                  assertEquals(new Object[] {"abc"}, new Object[] {"abc"});
              }
        ```
        - 테스트가 실패한므로 키를 위한 객체를 따로 만들어야 겠다.
        ```java
              Pair
              public class Pair {
                  private String from;
                  private String to;
                  
                  Pair(String from, Strint to) {
                      this.from = from;
                      this.to = to;
                  }
              }
        ```
        - 우린 Pair를 키로 쓸거 니깐 equals()와 hashCode()를 구현해야 한다.
        - **지금은 리팩토링하는 중에 코드를 작성하는 것이기 때문에 테스트를 작성하지는 않을 것이다.**
        - 우리가 이 리팩토링을 마치고 모든 테스트가 통과한다면, 그때 우리는 그 코드가 실제로 사용되었다고 생각할 수 있다.
        ```java
              Pair
              public boolean equals(Object object) {
                  Pair pair = (Pair) object;
                  return from.equals(pair.from) && to.equals(pair.to);
              }
        
              public int hashCode() {
                  return 0;
              }
        ```
        - 일단, 환율을 저장할 뭔가가 필요하다.
        ```java
              Bank
              private Hashtable rates = new Hashtable();
        ```
        - 환율을 설정할 수도 있어야 한다.
        ```java
              Bank
              public void addRate(String from , String to, int rate) {
                  rates.put(new Pair(from,to),new Integer(rate));
              }
        ```
        - 그리고 필요할때 환율을 얻어 낼수도 있어야 한다.
        ```java
              public int rate(String from , String to) {
                  Integer rate = (Integer) rates.get(new Pair(from, to));
                  return rate.intValue();
              }
        ```
        - 에러 발생. 이유는 USD에서 USD로의 환율을 요청하면 그 값이 1이 되어야 한다고 기대한다는 것을 알 수 있다.
        - **우리가 발견한 내용을 나중에 코드를 읽어볼 다른 사람들에게도 알려 주기 위해 테스트로 만들어 두자**
        ```java
           public void testIdentityRate() {
              assertEquals(1, new Bank().rate("USD", "USD"));
          }
        ```
       - 에러 수정
       ```java
          public int rate(String from , String to) {
              if (from.equals(to)) return 1;
              Integer rate = (Integer) rates.get(new Pair(from, to));
              return rate.intValue();
          }
       ``` 
       
    - 정리
        - 필요할 것라고 생각한 인자를 빠르게 추가했다.
        - 코드와 테스트 사이에 있는 데이터 중복을 끄집어 냈다.
        - 자바의 오퍼레이션에 대한 가정을 검사해보기 위한 테스트를 작성했다.
        - 별도의 테스트 없이 전용 (private, Hashtable) 도우미 (helper, Pair) 클래스를 만들었다.
        - 리팩토링 하다가 실수를 했고, 그문제를 분리하기 위해 또 하나의 테스트를 작성하면서 계속 전진해 가기로 선택했다.