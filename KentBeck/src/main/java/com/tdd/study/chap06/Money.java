package com.tdd.study.chap06;

import com.tdd.study.chap08.Dollar;

public class Money {
    protected int amount;

    @Override
    public boolean equals(Object object) {
        Money money = (Money)object;
        return amount == money.amount;
        //return true;
    }
}
