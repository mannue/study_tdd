package com.tdd.study.chap10;

public class Money {
    protected int amount;
    protected   String currency;

    public Money(int amount, String currency) {
        this.amount = amount;
        this.currency = currency;
    }

    public static Money dollar(int amount) {
        Money money = new Dollar(amount,"USD");
        return money;
    }

    public static Money franc(int amount) {
        return new Franc(amount,"CHF");
    }

    public boolean equals(Object object) {
        Money money = (Money) object;
        return amount == money.amount && currency().equals(money.currency());
    }

    public Money times(int multiplier) {
        return new Money(amount * multiplier, currency);
    }

      String currency() {
        return this.currency;
    }

    @Override
    public String toString() {
        return amount + " " + currency;
    }
}
