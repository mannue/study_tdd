#study_tdd
- 흥미로운 시간
    - 두 times() 구현이 거의 비슷하지만 아직 완전히 동일하지는 않다
        ```
             @Override
            Money times(int multiplier) {
                return Money.franc(amount * multiplier);
            }
            
            @Override
            Money times(int multiplier) {
                return Money.dollar(amount * multiplier);
            }
        ```
    - 이 둘을 동일하게 만들기 위한 명백한 방법이 없다.
    - 때로는 전진하기 위해서 물러서야 할 때도 있는 법이다.
    - 팩토리 메서드를 인라인 시키면 어떨까? 
        ```
             @Override
            Money times(int multiplier) {
                return Franc(amount * multiplier,"CHF");
            }
            
            @Override
            Money times(int multiplier) {
                return Dollar(amount * multiplier,"USD");
            }
        ```
    - "CHF" 와 "USD" 를 currency 멤버 변수로 변걍
        ```
             @Override
            Money times(int multiplier) {
                return Franc(amount * multiplier,currency);
            }
            
            @Override
            Money times(int multiplier) {
                return Dollar(amount * multiplier,currency);
            }
        ```
        - times() 에서 Franc 을 가질지 Money 를 가질지 시스템에 대해 아는 지식을 기반으로 조심스럽게 생각해 보아야 할 문제다.
        - 하지만 우리에겐 깔끔한 코드와, 그 코드가 잘 작동할거라는 믿음을 줄 수 있는 테스트 코드들이 있다.
        - 몇분 동안 고민하는 대신 그냥 수정하고 테스트를 돌려서 컴퓨터 에게 직접 물어보자!!
        - 가끔은 그냥 컴퓨터에게 물어보는 것도 좋다.
    - Franc.times() 에서 생성하는 클래스를 Money 로 변경
    - 빌드 에러 발생 , 이유는 추상 클래스는 객체를 생성 할수 없음으로 추상클래스에서 일반 클래스로 변경하고 times 메서드로 심플하게 구현
    - test 코드 실패 , 에러 관련 메시지를 보기 위해서 toString()을 정의
        - toString() 추가시 테스트 코드 작성해야 하는가?
            - *빨간 막대 상태인데 이 상테에서는 새로운 테스트를 작성하지 않는게 좋다*
            - toString() 은 디버그 출력에만 쓰이기 때문에 이게 잘못 구현됨으로 인해 얻게 될 리스크가 적다
    - 문제는 equals() 에서 class 비교시 Object 를 상위 클래스로 변경함으로서 서로 다른 클래스임으로 에러 발생
    - equals 에서 정말로 검사해야 할것은 클래스가 같은지가 아니라 currency 가 같은지 여부다.
    - *빨간 막대인 상황에서는 테스트를 추가로 작성하고 싶지 않는다* 보수적인 방법으로 변경된 코드를 되돌려서 다시 초록막대 상태로 돌아간다.
    - 초록 막대 상태로 된후 equals() 를 위해 테스트를 고치고 구현 코드를 고친다. 
        ```
            @Test
            public void testDifferentClassEquality() {
                assertTrue(new Money(10,"CHF").equals(new Franc(10,"CHF")));
            }
        ``` 
        - 실패 , equals() 에서 class 가 아닌 currency 비교로 변경
    - 다시 times() 의 객체 생성시 상위 클래스로 변경 후 테스트 빌드 
    - Dollar 클래스의 times() 도 객체 생성시 상위 클래스로 변경
    - 두 하위 클래스가 동일함으로 상위 클래스로 이동 
    - 정리
        1. 두 times() 를 일치시키기 위해 그 메서드들이 호출하는 다른 메서드들을 인라인 시킨후 상수를 변수로 바꿔주었다.
        2. 단지 디버깅을 위해 테스트 없이 toString()을 작성했다.
        3. Franc 대신 Money 를 반환하는 변경을 시도한 뒤 그것이 잘 작동할지를 테스트가 말하도록 했다.
        4. 실험해본 걸로 뒤로 물리고 또 다른 테스트를 작성했다. 테스트를 작동했더니 실험도 제대로 작동했다.
    
    