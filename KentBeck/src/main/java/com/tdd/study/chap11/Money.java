package com.tdd.study.chap11;

public class Money {
    private int amount;
    private String currency;

    public Money(int amount, String currency) {
        this.amount = amount;
        this.currency = currency;
    }

    public boolean equals(Object object) {
        Money money = (Money)object;
        return money.amount == amount && getCurrency().equals(money.getCurrency());
    }
    public static Money dollar(int amount) {
        return new Money(amount,"USD");
    }

    public static Money franc (int amount) {
        return new Money(amount,"CHF");
    }

    public Money times(int multiplier) {
        return new Money(amount * multiplier,currency);
    }

    public String getCurrency() {
        return currency;
    }
}
