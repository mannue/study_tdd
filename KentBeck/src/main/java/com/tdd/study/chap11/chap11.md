#study_tdd
- 모든 악의 근원
    - 두 하위 클래스 Dollar 와 Franc 에는 달랑 생성자밖에 없다.
    - 생성자 때문에 하위 클래스가 있을 필요는 없기 때문에 하위 클래스를 제거
    - 코드의 의미를 변경하지 않으면서도 하위 클래스에 대한 참조를 상위 클래스에 대한 찹조로 변경
    - Money.franc 과 Money.dollar 에서 생성시 클래스를 super 클래스로 변경
        ```
            public static Money dollar(int amount) {
                return new Money(amount,"USD");
            }
            
            public static Money franc (int amount) {
                return new Money(amount,"CHF");
            }
        ```
    - Dollar 에 대한 참조가 테스트 포함한 코드에도 없음으로 제거 
    - Franc 같은 경우 testDifferentClassEquality() 에서 참조하지만 다른 동치성 테스트에서 해당 테스트 코드를 커버해줌으로서 제거가능함
    - 또한 테스트 코드에서 중복되는 부분도 제거
    - 정리
        1. 히위 클래스의 속을 들어내는 걸 완료하고, 하위 클래스를 삭제했다.
        2. 기존의 소스 구조에서는 필요했지만 새로운 구조에서는 필요 없게 된 테스트를 제거했다.
    