package com.tdd.study.chap16;

public interface Expression {
    Money reduce(Bank bank, String to);
    Expression plus(Expression tenFrancs);
    Expression times(int multiplie);
}
