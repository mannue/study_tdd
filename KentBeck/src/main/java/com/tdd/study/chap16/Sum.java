package com.tdd.study.chap16;

public class Sum implements Expression {
    Expression augend;
    Expression addend;

    public Sum(Expression augend, Expression addend) {
        this.augend = augend;
        this.addend = addend;
    }

    @Override
    public Money reduce(Bank bank, String to) {
        int amount = augend.reduce(bank,to).amount + addend.reduce(bank,to).amount;
        return new Money(amount, to);
    }

    @Override
    public Expression plus(Expression tenFrancs) {
        return new Sum(this,tenFrancs);
    }

    @Override
    public Expression times(int multiplie) {
        return new Sum(augend.times(multiplie),addend.times(multiplie));
    }
}
