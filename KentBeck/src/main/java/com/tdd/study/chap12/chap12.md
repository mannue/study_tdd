#study_tdd
- 드디어, 더하기
    ```java
         public void testSimpleAddition() {
             Money sum = Money.dollar(5).plus(Money.dollar(5));
             assertEquals(Money.dallor(10), sum)
         }
    ```
    - 그냥 'Money.dallor(10)' 를 반환하는 식으로 가짜 구현을 할수 도 있지만 어떻게 구현해야 할지 명확하므로 다음과 같이 하겠다.
    ```java
          Money
          Money plus(Money addend) {
              return new Money(amount + added.amount, currency); 
          }
    ```
    - 사려 깊게 생각해 봐야 할 때가 있고, 또 그런 테스트가 있게 마련이다.
    - 설계상 가장 어려운 제약은 다중 통화 사용에 대한 내용을 시스템의 나머지 코드에게 숨기고 싶다는 점이다.
    - TDD 는 적절한 때에 번뜩이는 통찰을 보장하지 못하지만 확인을 주는 테스트와 조심스럽게 정리된 코드를 통해, 통찰에 대한 준비와 함께 통찰이 번뜩일때 그걸 적용할 준비를 할 수있다.
    ```java
      public void testSimpleAddition() {
       assertEquals(Money.dollar(10), reduced);
      }
    ```
    - reduced 란 이름의 Expression 은 Expression 에 환율을 적용함으로써 얻어진다.
    ```java
      public void testSimpleAddition() {
          Money reduced = bank.reduce(sum, "USD");
          assertEquals(Money.dollar(10), reduced);
      }
    ```
    - 수식이 아닌 은행의 책임이어야 한다는 이유?
        1. Expression 은 우리가 하려고 하는 일의 핵심에 해당한다. 핵심이 되는 객체가 다른 부분에 대해서 될수 있는 한 모르도록 노력한다.
        그렇게  하면 핵심 객체가 가능한 오랫동안 유연할수 있다.
        2. Expression과 관련이 있는 오퍼레이션이 많을 거라고 상상할 수 있다. 만약 모든 오퍼레이션을 Expression 에만 추가한다면 Expression 은 무한히 커질 것이다. 
        
    ```java
      public void testSimpleAddition() {
            Bank bank = new Bank();
            Money reduced = bank.reduce(sum, "USD");
            assertEquals(Money.dollar(10), reduced);
       } 
    ```
    - 두 Money 의 합은 Expression 이어야 한다.
    ````java
       public void testSimpleAddition() {
              Money five = Money.dollar(5);
              Expression sum = five.plus(five);
              Bank bank = new Bank();
              Money reduced = bank.reduce(sum, "USD");
              assertEquals(Money.dollar(10), reduced);
         } 
    ````
    - 정리
        -  큰 테스트를 작은 테스트로 줄여서 발전을 나타낼수 있도록 했다.
        - 우리에게 필요한 계산에 대한 가능한 메타포들을 신중히 생각해봤다.
        - 새 메타포에 기반하여 기존의 테스트를 재작성했다.
        - 테스트를 빠르게 컴파일했다.
        - 그리고 테스트를 실행했다.
        - 진짜 구현을 만들기 위해 필요한 리팩토링을 약간의 전율과 함께 기대했다.
        