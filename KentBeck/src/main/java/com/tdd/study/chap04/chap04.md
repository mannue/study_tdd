#study_tdd
- 프라이버시 
    - 이제 동치성 문제를 정의했으므로 이를 이용하여 테스트가 조금 더 많은 이야기를 해줄수 있도록 만들자.
    - 값을 이용한 테스트를 객체로 대처함으로써 일련의 오퍼레이션이 아니라 참인 명제에 대한 단언들이므로 우리의 의도를 더 명확하게 이야기 해준다.
    - 만약 동치성 테스트가 동치성에 대한 코드가 정확히 작동한다는 것을 검증하는데 실패한다면, 곱하기 테스트 역시 곱하기에 대한
      코드가 정확하게 작동한다는 것을 검증하는 데 실패하게 된다. *이것은 TDD 하면서 적극적으로 괸리해야 할 위험 요소다*
    - 우리는 완벽함을 위해 노력하지 않으며 모든 것을 두번 말함으로써 (코드와 테스트로 한번씩) 자신감을 가지고 전진할 수 있을 만큼만 결함의 정도를 낮추기를 희망할 뿐이다.
    - 때때로 우리의 추론이 맞지 않아서 결함이 손가락 사이로 빠져 나가는 수가 있다. 그럴 때면 테스트를 어떻게 작성해야 했는지에 대한 교훈을 얻고 다시 앞으로 나아간다.
    - 정리
        - 오직 테스트를 향상시키기 위해서만 개발된 기능을 사용했다.
        - 두 테스트가 동시에 실패하면 망한다는 점을 인식했다.
        - 위험 요소가 있음에도 계속 진행했다.
        - 테스트와 코드 사이의 결합도를 낮추기 위해, 테스트하는 객체의 새 기능을 사용했다.