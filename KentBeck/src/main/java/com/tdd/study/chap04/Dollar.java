package com.tdd.study.chap04;

public class Dollar {
    private int amount;

    public Dollar(int amount) {
        this.amount = amount;
    }

    public Dollar times(int multiplier) {
        /* step 1
        amount *= multiplier;
        return null;*/

        // step2
        return new Dollar(amount * multiplier);
    }

    @Override
    public boolean equals(Object object) {
        Dollar dollar = (Dollar)object;
        return amount == dollar.amount;
        //return true;
    }
}
