#study_tdd
- 서로 다른 통화 더하기
    - $5 + 10CHF 에 대한 테스트를 추가할 준비가 됐다
        ```java
            @Test
            public void testMixedAddition() {
              Expression fiveBucks = Money.dollar(5);
              Expression tenFrancs = Money.franc(10);
              Bank bank = new Bank();
              bank.addRate("CHF","USD",2);
              Money result = bank.reduce(fiveBucks.plus(tenFrancs),"USD");
              assertEquals(Money.dollar(10),result);
            }
        ```
        - 우리가 원하는 코드이지만 불행히도 컴파일 에러가 무지 많다.
        - Money 에서 Expression 으로 일반화 할때 우리가 어설프게 남겨둔 것들이 꽤 된다.
        - 처음 코드 수정이 다음으로 계속해서 퍼져나 갈수 있도록 할 것이다.
        - 앞으로 나아갈 수 있는 두 갈래 길이 있다.
            1. 좁은 범위의 한정적인 테스트를 빠르게 작성한 후에 일반화 하는 방법
            2. 우리의 모든 실수를 컴파일러가 잡아줄 거라 믿고 진행하는 방법
        ```java
            @Test
            public void testMixedAddition() {
                Money fiveBucks = Money.dollar(5);
                Money tenFrancs = Money.franc(10);
                Bank bank = new Bank();
                bank.addRate("CHF","USD",2);
                Money result = bank.reduce(fiveBucks.plus(tenFrancs),"USD");
                assertEquals(Money.dollar(10),result);
            }
        ```
        - 테스트가 실패. 10USD 대신 15USD가 나온다.
        - Sum.reduce()가 인자를 축약하지 않는 것같아 보인다.
        ```java
              Sum
              public Money reduce(Bank bank , String to) {
                  int amount = augend.amount + addend.amount;
                  return new Money(amount, to);
              }
        ```
        - 두 인자를 모두 축약하면 테스트가 통과 할것 이다.
        ```java
              public Money reduce(Bank bank , String to) {
                  int amount = augend.reduce(bank,to).amount + addend.reduce(bank,to).amount;
                  return new Money(amount, to);
              }
        ```
        - **파급 효과를 피하기 위해 가장자리에서 작업해 나가기 시작해서 그 테스트 케이스까지 거슬러 올라오도록 하겠다.**
        - 피가산수와 가수는 이제 Expression 으로 취급할 수 있다.
        ```java
              Sum
              Expression augend;
              Expression addend;
        ```
        - Sum 생성자의 인자 역시 Expression 일수 있다.
        ```java
              Sum
              public Sum(Expression augend, Expression addend) {
                  this.augend = augend;
                  this.addend = addend;
              }
        ```
        - plus() 의 인자가 Expression 으로 취급될수 있다.
        ```java
              public Expression plus(Expression addend) {
                  return new Sum(this, addend);
              }
        ```
        - times() 의 반환 값도 Expression 일수 있다.
        ```java
              public Expression times(int multiplier) {
                  return new Money(amount * multiplier, currency);
              }
        ```
        - **이 코드는 Expression 이 plus() 와 times() 오퍼레이션을 포함해야 함을 제안하고 있다.**
        - 이제 테스트 케이스에 나오는 plus()의 인자도 바꿀 수있다.
        ```java
            @Test
            public void testMixedAddition() {
                Money fiveBucks = Money.dollar(5);
                Expression tenFrancs = Money.franc(10);
                Bank bank = new Bank();
                bank.addRate("CHF","USD",2);
                Money result = bank.reduce(fiveBucks.plus(tenFrancs),"USD");
                assertEquals(Money.dollar(10),result);
            }
        ```
        - fiveBucks 를 Expression 으로 바꾸고 나면 몇 군데를 같이 수정해야 한다.
        ```java
            @Test
            public void testMixedAddition() {
                Expression fiveBucks = Money.dollar(5);
                Expression tenFrancs = Money.franc(10);
                Bank bank = new Bank();
                bank.addRate("CHF","USD",2);
                Money result = bank.reduce(fiveBucks.plus(tenFrancs),"USD");
                assertEquals(Money.dollar(10),result);
            }
      
            Expression
            Expression plus(Expression tenFrancs);
        ```
        - 이제 Money 와 Sum 에도 추가해야 한다.
        - Money 의 파라미터 클래스를 변경!!
        ```java
           public Expression plus(Expression addend) {
               return new Sum(this, addend);
           }
        ```
        - Sum 의 그현을 스텁 구현으로 바꾸고, 할일 목록에 적어두자
        
    - 정리
        - 원하는 테스트를 작성하고, 한 단계에 달성 할 수 있도록 뒤로 물렀다.
        - 좀더 추상적인 선언을 통해 가지에서 뿌리로 일반화 했다.
        - 변경 후, 그 영향을 받은 다른 부분들을 변경하기 위해 컴파일러의 지시를 따랐다.
        