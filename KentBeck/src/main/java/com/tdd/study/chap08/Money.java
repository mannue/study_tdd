package com.tdd.study.chap08;

public abstract class Money {
    protected int amount;

    @Override
    public boolean equals(Object object) {
        Money money = (Money)object;
        return amount == money.amount && getClass().equals(money.getClass());
        //return true;
    }

     static Money dollar(int amount) {
        return new Dollar(amount);
    }
     static Money franc(int amount) { return new Franc(amount); }
    abstract Money times(int multiplier);
}
