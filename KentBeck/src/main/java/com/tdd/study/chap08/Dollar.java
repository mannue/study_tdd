package com.tdd.study.chap08;

public class Dollar extends Money {

    public Dollar(int amount) {
        this.amount = amount;
    }

    public Dollar times(int multiplier) {
        /* step 1
        amount *= multiplier;
        return null;*/

        // step2
        return new Dollar(amount * multiplier);
    }
}
