package com.tdd.study.chap13;

public interface Expression {
    Money reduce(String to);
}
