#study_tdd
- 사과와 오렌지
    - Franc 과 Dollar 동치성 테스트 진행 , 실패를 예상 했지만 성공함으로서 테스트 실패
        ```
            실패 이유?
            @Override
            public boolean equals(Object object) {
                Money money = (Money)object;
                return amount == money.amount;
                //return true;
            }
            # amount 만 같으면 True 이기 때문이다.
        ```
    - Money.equals 에 Class 비교문 추가
    - 위 비교문이 재정 분야에 맞는 용어를 사용해야 깔끔해 보임으로 할일에 추가 한다.
    - 정리
        1. 우리 괴롭히던 결함을 끄집어내서 테스트에 담아냈다.
        2. 오나벽하지 않지만 그럭저럭 봐줄만한 방법(getClass()) 으로 테스트를 통과하게 만들었다.
        3. 더 많은 동기가 있기 전에는 더 많은 설계를 도입하지 않기로 했다.
    
    