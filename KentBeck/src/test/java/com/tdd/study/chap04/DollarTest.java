package com.tdd.study.chap04;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class DollarTest {

    @Test
    public void testMultiplication() {
       Dollar five = new Dollar(5);
       assertEquals(new Dollar(10),five.times(2));
       assertEquals(new Dollar(15),five.times(3));
    }
}