import java.util.Objects;

public class Number {
    private int num;
    public Number(int num) {
        this.num = num;
    }

    @Override
    public boolean equals(Object obj) {
        Number number = (Number)obj;
        return this.num == number.num;
    }

    @Override
    public int hashCode() {
        return Objects.hash(num);
    }

    public int value() {
        return num;
    }
}
