import java.util.Arrays;
import java.util.List;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;

public class Calculator {
    public Number add(Number ... numbers) {
        checkInputParam(numbers);
        return process(Arrays.asList(numbers),new Number(0),(x,y)->x+y);
    }

    public Number times(Number ... numbers) {
        checkInputParam(numbers);
        return process(Arrays.asList(numbers),new Number(1),(x,y)->x*y);
    }

    public Number minus(Number ... numbers) {
        checkInputParam(numbers);
        return  process(Arrays.asList(numbers),new Number(numbers[0].value()*2),(x,y)->x-y);
    }

    private void checkInputParam(Number ... numbers) {
        if(numbers.length < 2 ) throw new IllegalArgumentException();
    }

    private Number process(List<Number> numbers,Number initNumber, BinaryOperator<Integer> binaryOperator) {
        return  new Number(numbers.stream().collect(Collectors.reducing(initNumber.value(), Number::value, binaryOperator)));
    }
}
