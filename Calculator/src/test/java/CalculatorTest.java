import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;


@RunWith(JUnit4.class)
public class CalculatorTest {

    @Test
    public void testAddNumber() {
        Number tree = new Number(3);
        Number four = new Number(4);
        Calculator calculator = new Calculator();
        Number result = calculator.add(tree, four);
        assertThat(result.value(),is(7));
    }

    @Test
    public void testMultiAddNumber() {
        Number tree = new Number(3);
        Number four = new Number(4);
        Calculator calculator = new Calculator();
        Number result = calculator.add(four, calculator.add(tree, four));
        assertThat(result.value(),is(11));
    }


    @Test
    public void 곱하기() {
        Number tree = new Number(3);
        Number four = new Number(4);
        Calculator calculator = new Calculator();
        Number result = calculator.times(tree,four);
        assertThat(result.value(),is(12));
    }


    @Test
    public void 곱하기더하기테스트() {
        Calculator calculator = new Calculator();
        Number result = calculator.times(new Number(4),calculator.add(new Number(3),new Number(4)));
        assertThat(28,is(result.value()));
    }

    @Test
    public void 빼기() {
        Number tree = new Number(3);
        Number four = new Number(4);
        Calculator calculator = new Calculator();
        Number result = calculator.minus(tree,four);
        assertThat(result.value(),is(-1));
    }

    @Test
    public void 연속빼기() {
        Number tree = new Number(3);
        Number four = new Number(4);
        Calculator calculator = new Calculator();
        Number result = calculator.minus(calculator.minus(tree, four), tree);
        assertThat(result.value(),is(-4));
    }

    @Test
    public void 음수빼기(){
        Calculator calculator = new Calculator();
        Number result = calculator.minus(new Number(-3), new Number(-4));
        assertThat(result.value(),is(1));
    }

    @Test
    public void 연속음수빼기() {
        Calculator calculator = new Calculator();
        Number result = calculator.minus(calculator.minus(new Number(-3), new Number(-4)), new Number(-3));
        assertThat(result.value(),is(4));
    }

}
