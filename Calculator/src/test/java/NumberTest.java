import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class NumberTest {

    @Test
    public void testCreateNumber() {
        Number tree = new Number(3);
        assertThat(3,is(tree.value()));
    }

    @Test
    public void testEqaulsNumber() {
        Number tree = new Number(3);
        assertThat(tree,is(new Number(3)));
    }

    @Test
    public void testDifferentNumber() {
        assertNotEquals(new Number(3),new Number(4));
    }

    @Test
    public void 동일한값테스트() {
        assertEquals(new Number(3),new Number(3));
    }
}