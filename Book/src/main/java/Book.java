public class Book {
    private String name;
    private Integer price;
    private Integer discount;


    public Book(String name, Integer price, Integer discount) {
        this.name = name;
        this.discount = discount;
        this.price = price;
    }

    public static Book PaperBook(String name, int price) {
        return new Book(name,price,0);
    }

    public static Book EBook(String name, int price) {
        return new Book(name,price,20);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof  Book) {
            Book book = (Book) obj;
            return this.name.equals(book.name) && this.getPrice().equals(book.getPrice());
        }
        return false;
    }

    public Integer getPrice() {
        return (price * (100 - discount)) / 100;
    }
}
