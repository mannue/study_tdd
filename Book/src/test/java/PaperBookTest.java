import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;


public class PaperBookTest {

    @Test
    public void 책은이름과가격을가진다() {
        Book paperBook = Book.PaperBook("TDD",10000);
        assertEquals(paperBook,Book.PaperBook("TDD",10000));

        Book eBook =  Book.EBook("TDD",10000);
        assertEquals(eBook,Book.EBook("TDD",10000));

        assertNotEquals(paperBook,eBook);
    }

    @Test
    public void 종이책과ebook은같은책이여도가격이다르다() {
        Book eBook = Book.EBook("TDD",10000);
        Book paperBook = Book.PaperBook("TDD",10000);
        assertNotEquals(eBook.getPrice(),paperBook.getPrice());
    }

}
