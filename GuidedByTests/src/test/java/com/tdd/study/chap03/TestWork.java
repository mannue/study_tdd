package com.tdd.study.chap03;

public class TestWork {
    private TestObject testObject;

    public TestWork(TestObject testObject) {
        this.testObject = testObject;
    }

    public void setTestObject(TestObject testObject) {
        this.testObject = testObject;
    }

    public String getId() {
        return testObject.getId();
    }
}
