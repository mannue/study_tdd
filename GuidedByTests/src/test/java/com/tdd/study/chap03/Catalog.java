package com.tdd.study.chap03;

import java.util.ArrayList;
import java.util.List;

public class Catalog {
    private final List<Entry>  entryList = new ArrayList();
    public void add(Entry entry) {
        entryList.add(entry);
    }

    public boolean cotains(Entry entry) {
        return entryList.contains(entry);
    }

    public int size() {
        return entryList.size();
    }
}
