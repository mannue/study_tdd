package com.tdd.study.chap03;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static junit.framework.TestCase.assertTrue;

@RunWith(JUnit4.class)
public class CatalogTest {
    private final Catalog catalog = new Catalog();
    @Test public void containAnAddedEntry() {
        Entry entry = new Entry("fish","chops");
        catalog.add(entry);
        System.out.println(catalog.size());
        assertTrue(catalog.cotains(entry));
    }
    @Test public void indexesEntriesByName() {
        Entry entry = new Entry("fish","chips");
        catalog.add(entry);
        System.out.println(catalog.size());
    }
}
