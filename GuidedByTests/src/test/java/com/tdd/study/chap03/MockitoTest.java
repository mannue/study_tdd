package com.tdd.study.chap03;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.*;

public class MockitoTest {
    List mockedList;
    @Before public void init() {
        mockedList = mock(List.class);
    }

    @Test public void checkMethodCall() {
// mock 객체 사용
        mockedList.add("one");
        mockedList.add("two");

// 검증 하기
        verify(mockedList).add("one");
        verify(mockedList).clear();
    }

    @Test public void checkReturnValue() {
        when(mockedList.size()).thenReturn(2);

        System.out.println(mockedList.size());
    }

    @Test public void countByMethodCall() {
        mockedList.add("once");

        mockedList.add("twice");
        mockedList.add("twice");

        mockedList.add("three times");
        mockedList.add("three times");
        mockedList.add("three times");

        mockedList.add("test");
// 아래의 두 가지 검증 방법은 동일하다. times(1)은 기본값이라 생략되도 상관없다.
        verify(mockedList).add("once");
        verify(mockedList, times(1)).add("once");

// 정확히 지정된 횟수만큼만 호출되는지 검사한다.
        verify(mockedList, times(2)).add("twice");
        verify(mockedList, times(3)).add("three times");

// never()를 이용하여 검증한다. never()는 times(0)과 같은 의미이다.
        verify(mockedList, never()).add("never happened");

// atLeast()와 atMost()를 이용해 검증한다.
        verify(mockedList, atLeastOnce()).add("three times");
        verify(mockedList, atLeast(2)).add("five times");
        verify(mockedList, atMost(5)).add("three times");
    }

    @Test
    public void getObject() {
        TestObject mock = mock(TestObject.class);
        when(mock.getId()).thenReturn("value");

        TestWork test = new TestWork(mock);

        String id = test.getId();

        assertThat(id,is("value"));

    }
}
