#study_tdd
- 동작하는 골격
    1. 골격 사용 준비
        - 가장 먼저 해야 할일은 *동작하는 골격*을 만드는 것이다
        - 동작하는 골격의 핵심은 대략적인 시스템 구조를 제안하고 그것의 유효성을 검증 할 수 있을 정도로 요구사항을 이해하는데 이바지하는 것이다.
        - 프로젝트에서는 대부분 동작하는 골력을 개발하는데 놀아울 만큼 노력이 든다.
    2. 최초 테스트
        - 동작하는 골격은 반드시 경매 스나이퍼 시스템의 모든 컴포넌트, 즉 사용자 인터페이스, 스나이퍼 컴포넌트 , 경매 서버와의 통신 등을 포괄해야 한다.
        - 테스트를 생각해 볼수 있는 가장 작은 부분이자 할일 목록의 첫 항목은 , 경매 스나이퍼는 결매에 참여한 후 경매가 종료 될때 까지 대기 할 수 있다는 것이다.
        - 이 부분은 아주 작아서 우리는 입찰을 보내는 것에 대해 신경쓰지 않으며, 단지 양측에서 서로 통신할수 있고 외부에서 시스템을 테스트 할수 있는지만 알고 싶다.
        - 우리는 구현이 이미 존재하는 것처럼 테스트를 작성한 다음 해당 구현이 동작하는데 필요한 것을 채워나가는 식으로 시작하는 방식을 선호한다.
        - 테스트를 가지고 역으로 작업하면 시스템을 어떻게 동작하게 만들지에 관련한 복잡함 때문에 발목이 잡히는 대신 *시스템이 해야 할일에 집중*하는데 도움이 된다
        - 테스트를 제어해 마치 실제 서비스터럼 동작하는 가짜 경매 서비스가 필요하며, 이러한 가짜 경매, 즉 스텁은 최대한 단순하게 만들것이다.
        - 사우스비 온라인의 모든 것을 다시 구현하자는 것이 아니며 테스트 시나리오를 보조할 수 있을 정도만 구현할것이다.
        