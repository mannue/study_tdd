#study_tdd
- 객체 지향 설계의 달성
    1. 테스트를 먼저 작성하는것이 설계에 어떻게 도움이 되는가
        - 호출자는 *객체가 무슨일을 하고 무엇에 의존하는지 알고 싶어 하지*, 해당 객체가 어떻게 동작하는지는 알고 싶어 하지 않는다.
        - 해당 객체를 둘러싼 더 큰 환경과 조화되는 응집력 있는 단위를 나타내기를 바라며, 그러한 컴포넌트로 만들어진 시스템은 요구 사항이 변경 될때 재구성과 적응에 필요한 유연함을 갖출 것이다.
        - TDD 의 3가지 측면
            1. 테스트로 시작한다 는것은 '어떻게'를 고려하기 전에 *달성하고자 하는 바가 '무엇인지'를 기술*해야 함을 의미한다.
                - 대상 객체에 대해 추상화를 올바른 수준으로 유지하는 데 기여한다.
                - 객체 외부에서 무엇을 볼 수 있는지도 결정해야 하므로 정보은닉에도 도움이 된다.
            2. 단위 테스트를 이해 가능한 상태로 유지하려면(유지 보수할 수 있게) 단위 테스트의 범위를 제한해야 한다.
                - 테스트 요점이 테스트가 시작되는 부분 어딘가에 묻혀버리는 경우 태스트 대상 컴포넌트의 규모가 너무 커서 좀 더 작은 컴포넌트로 쪼개야 함을 말함
                - 그 결과로 만들어지는 복합 객체는 관심의 분리가 더 명확해질 테고, 추출한 객체를 대상으로 좀 더 단순한 테스트를 작성 할 수 있다.
            3. 단위 테스트를 위한 객체를 만들려면 해당 객체의 의존성을 전달해야 하는데, 그러한 의존성이 어디에 있는지 알아야 한다는 의미
                - 콘텍스트 독집성이 높아진다.
                - 의존성이 암시적인 객체는 테스트를 준비하기 너무 어려우므로 의존성을 정리하는 작업이 유의미하다는 사실을 알게 될것 이다.
    2. 분류 보다 의사소통
        - 시스템은 객체의 망으로 구성 되어 있음으로 객체들간의 필요한 기능에 관한 부분에 설계 노력을 집중한다.
        -  자바 같은 언어에서는 인터페이스를 사용해 객체끼리 전달 가능한 메시지를 정의할 수 있지만 인터페이스나 메서드 간의 관계를 기술 할만 한것이 없어서 설계의 상당부분이 암시적인 상태로 남는다.
        ```
            인터페이스 와 프로토콜
            - 인터페이스는 두 컴포넌트가 잘 맞는지를 기술하는 반면, 프로토콜은 두 컴포넌트가 함께 동작하는지를 기술 
            - 프로토콜은 유사한 기능 , 인터페이스는 서로 다른 기능
        ```
        - 목객체를 활용한 TDD 를 이용하면 개발 과정에서 통신 프로토콜을 발견하는 수단인 동시에 코드를 볼때 해당 코드의 설명으로도 작용
        ```
            ex) 3장에서 translator 는 listener.auctionClosed() 매서드를 딱 한번만 호출해야 함을 말해준다.
        ```
        - 목객체를 활용한 TDD 는 정보 은닉을 촉진하며, 목 객체는 의존성 , 알림 ,조정을 대상으로 만들어야 한다.
        - 한객체의 인접 요소를 강조하는 테스트는 그것들이 이웃 요소인지 아니라면 대생 객체의 내부를 구성해야 하는지 파악하는데 기여한다.
        - 어설프거나 불분명한 테스트는 구현 세부 사항을 너무 드러냈으며 객체와 이웃 간의 책임을 다시 균형되게 만들어야 한다는 징표일지도 모른다.
    3. 값 타입
        - 시스템에서 String 을 사용하기 보다는 Item 타입을 만든다면 메서드 호출로 추적할 필요 없이도 변경과 관련된 코드를 모두 찾아낼 수 있다.
        - 구체적인 타입은 혼동의 위험을 줄일 수 있다.
        - 개념을 표현하는 타입을 만들어두면 좀 더 객체 지향적ㅇ니 접근법을 사용하는데 도움이 된다.
        - 값 타입을 도입할 때 3 가지 기본적인 기법을 활용
            1. 분해 
                - 어떤 객체의 코드가 복잡해지고 있다면 해당 코드가 다수의 관심사를 구현 하고 있으니 이를 *응집력 있는 행위의 단위로 분해해 도우미 타입으로 만들어내라는 신호*
            2. 파생
                - 코드에 새로운 도메인 개념을 표시하고 싶을 때 필드 하나만 있거나 필드가 아무것도 없는 placeholder 타입을 도입한다.
            3. 포장
                - 항상 함께 사용하는 부분이 있다면 하나의 그룹으로 생성해야한다.     
    
    4. 객체는 어디에서 오는가 ?
        1. 분해 (역할 분리)
            - 작성한 코드가 이해 할 수 없을 정도로 너무 복잡해지면 응집력 있는 기능 단위로 좀 더 작은 협력 객체로 만드는 작업을 시작하고
               해당 작업 후에는 독립적으로 단위 테스트를 할 수 있다.
            - 새 객체로 나누는 과정에서 끄집어 내려는 코드의 의존성을 살펴 볼수 밖에 없다.   
            - 정리 작업중 우려 사항
                - 시간 압박이 있는 상황에서는 구조가 제대로 잡하지 않는 코드를 그 상태 그대로 놔두고 다음 작업으로 넘어가려는 유혹에 빠지기 쉽다.
                - 너무 복잡해지면 시험용 코드로 여기고 깔끔하게 다시 구현하는 편이 낫다
            ```
                어떤 객체가 테스트 할수 없을정도로 몸집이 크거나 테스트가 실패한 이유를 해석 하기 어렵다면 해당 객체를 분해한다.
                그러나서 새로 나눈 부분들을 따로따로 단위 테스트 한다.
            ```
        2. 파생 (의존성)
            - 객체가 필요로 하는 신규 서비스 정의와 해당 서비스를 제공하기 위한 새 객체 추가
            - 필요한 서비스가 생기면 인터페이스를 만들어 객체 관점에서 필요한 서비스를 정의하고 대상 객체와 협력 객체 간의 관계를 기술하는데 도움이 되는 목 객체를 이용해 
              서비스가 이미 존재하는 양 새로운 행위에 대한 테스트를 작성한다.
              ```
                ex) AuctionEventListener 도 이와 같은 식으로 도입
              ```  
            - 개발 주기
                - 어떤 객체를 구현할때 그 객체가 또 다른 객체에서 제공받아야 할 서비스를 필요로 한다는 사실을 발견
                - 새로운 서비스에 이름을 클라이언트 객체와 새로운 서비스 간의 관계를 명확히 하고자 해당 서비스를 클라이언트 객체의 단위 테스트 바깥에서 목객체 로 만들어 제공
                - 그 서비스를 제공하는 객체를 작성하는데, 이 과정에서 해당 객체가 필요로 하는 서비스가 뭔지 파악한다.
                - 기존 객체, 또는 자체적으로 만든 API 나 서드 파트의 API 와 연결하기 전까지 이처럼 협력 객체의 관계로 구성된 흐름을 따른다.
        3. 포장
            - "전체는 부분의 합보다 단순해야 한다" 규칙의 응용이다.
            - 함께 동작 하는 관련 객체의 집합이 있는 경우 그것들을 하나로 포함하는 객체로 포장 할 수 있다.
            - 새 객체는 기존 집합의 복잡성을 추상화 너머로 감춰서 좀 더 높은 수준에서 프로그램을 작성 할 수 있게 해준다.
            - 암시적인 개념을 구체적으로 만드는 과정은 다른 좋은 효과도 있다.
                - 도메인을 좀 더 잘 이애하는데 보탬이 되는 이름을 개념에 부여해야 한다.
                - 개념의 경계를 확인 할수 있으므로 의존성의 범위를 좀 더 명확하게 한정 할수 있다.
                - 단위 테스트를 좀 더 정확하게 수행 할 수 있으며, 새로운 구성 객체를 직접 테스트 할수 있고,뽑아낸 객체의 코드에 대한 테스트를 단순화하는데 목 구현제를 쓸수 있다.
            ```
                어떤 객체의 테스트가 너무 복잡해서 수행할 준비를 할 수 없는 경우, 코드를 적절한 상태에 두기엔 유동적인 부분(객체 생성시 협력객체 주입하는 부분이)이 너무 많다면 협력 객체의 일부를 포장하는 방법을 고려해 본다
            ```         
    5. 인터페이스로 관계를 식별하라
        - 인터페이스를 사용해 객체가 수행할 수 있는 역할에 이름을 부여하고 객체가 받아들일 메시지를 기술한다.
        - 인터페이스에 선언되는 메서드 수가 적을수록 해당 메서드를 호출하는 객체의 역할이 명확해진다.
        - 인터페이스의 범위가 좁으면 어댑터와 데코레이터 를 작성하기도 쉽고 구현할 사항이 적어서 서로 잘 구성되는 객체를 작성하기 쉽다.
        - 클라이언트 코드에서 인터페이스를 쓰게 하면 해당 인터페이스의 구현에 관한 불필요한 정보가 새는 현상을 방지 할 수 있어 객체 간의 암시적인 결합이 최소화되고 코드는 변경하기 쉬운 상테에 머물게 된다.
    6. 인터페이스도 리팩토링하라
        - 프로토콜에 대한 인터페이스를 갖추고 나면, 유사점과 차이점에 신경 쓰기 시작할 수 있다.
        - 적당한 규모의 코드 기반에는 비슷한 것처럼 보이는 인터페이스를 찾는것부터 시작한다.
        - 인터페이스가 단일 개념을 표현하고 있으며, 병합될 수 잇는지 살펴야 함을 의미한다.
        - 비슷하게 보이는 인터페이스를 분리하기로 했다면 해당 인터페이스의 이름을 재고 해 보기에 좋은 시점이다.
        - 구현하는 클래스의 구조가 불분명하다는 사실을 알게 된다면 아마 해당 인터페이스에 책임이 너무 많을테고 이는 인터페이스가 너무 장황해서 쪼개야 한다는 힌트일지도 모른다.
    7. 객체를 구성해 시스템의 행위를 기술하라
        - 단위 수준의 TDD 는 시스템을 값 타입과 느슨하게 결합된 계산 관련 객체로 분해하는데 길잡이 노릇을 한다.
        - 테스트는 각 각체가 어떻게 행동해야 하고 다른 객체와 어떻게 결합될 수 있는지 이해 하는데 보탬이 된다.
        - 테스트가 실행되는 동안 Mockery 는 목 객체의 대상이 되는 객체를 상대로 발생한 호출을 Expectation(예상) 에 전달하고 , 각 Expectation 에 서는 호출이 일치하는지 검사하려고 할것 이다.
        - Expectation 이 일치하면 그 부분의 테스트는 성공한다.
    8. 고수준 프로그래밍을 위한 대비
        - 우리는 코드를 두 가지 계층으로 구성한다
            1. 객체의 그래프 에 해댕하는 구현 계층 으로 이 계층의 행위는 해당 계층에 속하는 객체가 이벤트에 어떻게 반응하는가로 결정
            2. 구현 계층의 객체를 만들어 내는 선언적 계층 으로 자그마한 편의성 메서드와 문법을 이용해 각 부문의 용도를 기술
            - 선언 계층은 코드에서 하려는 일이 무엇인지를 기술한다
            - 구현 계층에서는 코드가 그것을 어떻게 하는지를 기술하며 , 관례적인 객체 지향 스타일 지침을 고수
        - 결국 목표는 더 적은 코드로 더 많은 일을 해내는 것이며, 스스로가 제어 흐름과 데이터 조작이라는 측면의 프로그래밍에서 더 작은 프로그래밍을 가지고 
          프로그램을 구성하는 , 즉 객체가 더 작은 행위의 단위를 형성하는 수준에 이르기를 갈구한다.
    9. 그럼 클래스는 ?
        - 우리는 클래스 보다 인터페이스를 강조하는데, 다른 객체에서 보는 것은 결국 인터페이스이기 때문이다.
        - 객체의 타입은 해당 객체가 수행하는 역할로 규정된다.
        - 우리는 객체에 대한 클래스를 '구현 세부 사항' 으로 본다.
        - *클래스는 타입을 구현하는 한 방식이지, 타입 자체가 아니다.*
        - 우리는 동통적인 행위를 추려냄으로써 객체 클래스 계층 구조를 파악하며, 가능하다면 위임으로 리팩터링하는 방식을 선호하는데
          위임으로 코드를 더 유연하고 이해하기 쉽게 만들 수 있기 때문이다.
                  
                
            