#study_tdd
- 테스트 주도 개발 주기의 유지
    ```
    5장의 목표
    - TDD 프로세스가 어떻게 운영되는지 
    - TDD 프로세를 매끄럽게 진행하는 상세한 방법
        1. 시스템을 구축 할때 테스트를 작성하는 방법
        2. 테스트를 이용해 내외적인 품질 문제에 관해 피드백을 받는 방법
        3. 테스트가 계속 변화를 뒷받침하고 이후 개발에 걸림돌이 되지 않게 하는 방법
    ``` 
    1. 각 기능을 인수 테스트로 시작하라
        - *인수 테스트를 작성 할때 는 기반 기술의 용어가 아닌 응용 도메인에서 나온 용어만 이용하면 시스템에서 해야 할일이 뭔지 이해하는데 보탬이 된다.*
        - 시스템의 기술 기반 구조가 바뀌었을 때도 인수 테스트를 보호 할수 있다.
        - 코딩을 시작하기 전에 테스트를 작성하면 달성하고자 하는 바가 명확해진다
        - 실패하는 테스트 덕에 요구사항을 충족하는데 필요한 만큼이 기능만 구현하는데 집중 할수 있어 기능을 완성할 가능성이 높아진다.
        - *테스트를 시작하면 사용자 관점에서 시스템을 바라보게 되어 구현자 관점에서 기능을 짐작하지 않고 사용자가 필요로 하는 것을 이해하게 된다*
        - 단위 테스트는 객체나 작은 객체 집합을 격리된 상테에서 시험한다.
        - *단위 테스트는 클래스틑 설계하고 그것들이 동작한다는 확신을 주는데 이바지한다는 점에서 중요하자만 그 클래스가 시스템의 나머지 부분과 조화롭게 동작할지에 대해서는 아무것도 담보하지 않는다.(통합테스트 에서 확인가능)*
        - 인수테스트는 단위 테스트를 거친 객체를 대상으로 통합 테스트를 수행할뿐 아니라 프로젝트를 앞으로 나아가게 한다.
    
    2. 회귀를 포착하는 테스트와 진행 상항을 측정하는 테스트를 분리하라
        - 인수 테스트를 빨간색에서 녹색으로 바꾸는 활동으로 팀에서는 진행 상태를 측정 할수 있다.
        - *인수 테스트를 통과하면 이제 해당 테스트는 완료된 기능을 나타내고 다시는 실패해서는 안되며, 실패는 이전 상태로 회구했고 기존코드를 망가뜨렸음을 의미한다.*
        - 단위 테스트와 통합 테스트는 개발 팀을 보조하고 빠르게 실행되야 하며, 항상 통과해야 한다.
        - 요구 사항이 바뀌면 기존의 인수 테스트를 회귀 테스트 그룹에서 빼내서 진행 중인 테스트 그룹으로 옮긴 후 새 요구 사항을 반영토록 수정한 다음 해당 테스트를 다시 통과하게끔 시스템을 변경해야 한다.
    
    3. 테스트를 가장 간단한 성공 케이스로 시작하라
        - 상투적인 케이스는 시스템에 가치를 별반 더하지 않으며, 아이디어의 유효성에 관해 충분한 피드백을 전해주지 않는다
        - *성공 케이스로 한 테스트가 동작하면 솔루션의 실제 구조에 관해 더 좋은 생각이 떠오를 테고 , 그 과정에서 발견한 발생 가능한 실패를 처리하는것과 성공 케이스 사이에서 우선 순위를 가늠해 볼수 있다.*
        - *처리해야 할 실패 케이스,리팩터링, 기타 기술적인 작업을 메모장이나 색인 카드에 기록해 두면 도움이 된다*
        - 기록을 하면 세부 사항을 빠뜨리자 않고도 당면 과제에 집중 할수 있다.
        - 기능은 기록해둔 목록의 항목에 모두 선을 그러야 비로소 마무리된다.
    
    4. *읽고 싶어 할 테스트를 작성하라*
        - 각 테스트는 *시스템이나 객체에서 수행할 행위*로 가능한 한 명확하게 표현 
        - 테스트를 작성하는 동안에는 테스트가 실행 , 컴파일 되지 않으리 라는 사실을 무시하고 테스트 내용에만 집중한다.
        - 테스트가 잘 읽히면 테스트를 그 다음으로 지원하는 기반 구조를 만든다.
        
    5. 테스트가 실패하는 것을 지켜보라
        - 항상 테스트를 통과하는 코드를 작성하기 전에 테스트가 실패하는 것을 지켜본 후 진단 메세지를 확인한다.
        - *테스트가 예상과 다른식으로 실패하면 뭔가를 잘못 이해했거나 코드가 완성되지 않았다는 뜻이므로 부족한 부분을 고친다.*
        - 오류메시지가 코드와 관련된 문제로 우리를 이끌때까지 테스트 코드를 조정하고 테스트를 재실행한다.
        - 제품 코드를 작성할때는 테스트를 계속 실행해 진행 상태를 확인하고 시스템이 테스트 범위에 속하지 않을 경우 오류 진단 정보를 확인 
        - *필요한 부분에 대해서는 보조 코드를 확장하거나 수정해서 오류 메시지를 늘 명확하게 의미 있게 만든다.*
        - 오류 메시지를 검사해야 하는 이유
            1. 현재 작업 중인 코드에 대한 가정을 확인한다.
            2. 의도에 대한 표현을 강조 하는것은 신뢰성 있고 유지하기 쉬운 시스템을 개발하는 데 필수
                      
    6. 입력에서 출력 순서로 개발하라
        - *기능 개발을 시작할때 가장 먼저 시스템이 새로운 행위를 일으키게 하는 이벤트를 고려하며, 기능에 대한 전 구간 테스트는 이 이벤트가 도착하는것을 흉내 낼것이다.*
        - 시스템 경계에서는 이 이벤트를 다루고자 객체를 여러개 작성해야 할것이며, 객체들이 자기 책임을 수행하고자 시스템의 나머지 부분에 존재하는 서비스를 지원할 필요가 있음을 알게 됨
        - *외부 이벤트를 받는 객체에서 중간 계층을 거쳐 중심 도메인 모델로 나아간 다음, 외부에서 확인 가능한 응답을 생성하는 다른 경계에 위치한 객체에 까지 이른다.*
        - 새 도메인 모델 객체에 단위 테스트를 수행한 다음 애플리케이션의 나머지 부분에 해당 객체를 끼워 넣는 식으로 시작하고 싶을 수도 있지만 나중에 통합 문제로 골머리를 앓을 가능성이 높다.
        
    7. *메서드가 아닌 행위를 단위 테스트 하라*
        - 테스트를 많이 작성하고 테스트 커버리지가 높아도 다루기 쉬운 코드 기반이 만들어지지는 않는다
        - *메서드 테스트를 생각하면서 테스트 작성 시 나중에 되돌아 봤을때 이해하기 어렵다*
        - 테스트 대상 객체에서 제공해야 하는 기능에 집중 할때 더 잘할수 있다.
        - 해당 클래스의 코드를 통해서 이웃 객체와의 협업이나 여러 메서드를 호출이 필요로하는 모든 실행 경로를 시험하는 방법이 아니라 그 클래스로 목료를 당성하는 방법을 알아야한다.
        ```
            API 기능이 아닌 행위를 기술하는 것의 중요성
            - API 기능으로 테스트를 작성 시 테스트를 이해하기 어렵고 각 객체의 동작 방식, 
              즉 객체의 책임이 무엇이고 객체의 여러 메서드가 함께 어떻게 동작하는지 이해할 수 없다 
        ```
        - *테스트 이름을 지을 때는 테스트 중인 시나리오에서 객체가 어떻게 동작하는지 설명하는 이름을 선택하는 것이 도움이 된다.*
         
    8. 테스트에 귀를 기울이라
        - 테스트 하기 어려운 기능을 발견할때 해당 기능을 어떻게 테스트 할지 뿐만 아니라 그것이 왜 테스트 하기 어려운지 자문해 본다.
        - *경험상 코드가 테스트 하기 어렵다면 주로 설계 개선이 필요하기 때문이다. 즉 지금 당장 코드를 테스트 하기 어렵게 만드는 구조 때문에 나중에도 코드를 변경하기 어려울것이다.*
        - 실패할 테스트를 작성하기 어렵다면 제품 코드의 설계를 다시 살펴보고 앞으로 나아가기 전에 리팩터링 한다.
        - 설계에 취약함이 보일 때 리팩터링을 통해 시스템 품질을 유지한다면 어떠한 변화가 일어나도 거기에 대응 할수 있을 것이다.
       
    9. 주기의 미세조정
        - 너무 큰 단위로 테스트를 수행하면 코드의 모든 가능한 경로를 시도하는 조합 폭발 현상으로 개발이 중단 될것이다.
        ```
            조합 폭발
            - 매번 새로운 가족이나 변형을 지원하려고 새로운 클래스를 추가할 필요가 있을때 이러한 악취가 발생
        ```
        - 테스트 단위를 너무 세실하게 잡으면 테스트 하기는 쉽지만 함께 동작하지 않는 객체에서 유래하는 문제를 놓치고 말것 이다.     
               
  