#study_tdd

- 도구 소개
    1. Junit 4 소개
        1. 테스트 케이스
            - 기본적으로 JUnit 은 리플렉션을 통해 클래스 구조를 파악한 후 해당 클래스 내에서 테스트를 나타내는 것을 모드 실행한다.
            - JUnit 에서는 테스트를 실행하기 위해 테스트 클래스의 새 인스턴스를 생성한 후 적절한 테스트 메서드를 호출한다.
        2. 단정
            - assertTrue() : 표현식이 참임을 단정
            - assertNull() : 객체 참조가 null 임을 단정
            - assertEquals() : 두 값이 동일함을 단정
        3. 예외 예상하기
            - expected 는 테스트 케이스에서 예외를 던질 것으로 선언한다.
            ```
                @Test(expected=IllegalArgumentException.class)
                public void cannotAddTwoEntriesWithTheSameName() {
                    catalog.add(new Entry("fish","chips"));
                    catalog.add(new Entry("fish","peas"));
                }
            ```  
        4. 테스트 픽스처 
            - 테스트 픽스처는 테스트가 시작할때 존재하는 고정된 상태를 의미한다.
            - 같은 클래스에 정의된 테스트는 모두 동일한 픽스처를 가지고 시작하며, 실행될때 해당 픽스처를 변경해도 된다.
            - 픽스처는 테스트가 실행되기전에 준비해서 테스트 실행이 완료된 후에 정리할 수 있다.
            - JUnit 은 픽스처를 준비하기 위해 테스트 를 실행하기 전에 @Before 라는 애노테이션이 지정된 메서드를 모두 실행하고, 픽스처를 정리 하기 위해 테스트가 실행된 후 @After 라는 애노테이션이 지정된 메서드를 실행
            ```
                public class CatalogTest {
                    final Catalog catalog = new Catalog();
                    final Entry entry = new Entry("fish","chips");
                    
                    @Before public void fillTheCatalog() {
                        catalog.add(entry);
                    }
                }
            ```
            - 위 코드에서의 테스트 픽스처는 catalog 와 entry 이며 테스트 실행전에 @Before 를 먼저 동작하여 catalog 에 entry 를 삽입한다.
        5. 테스트 러너 
            - JUnit 이 클래스를 대상으로 리플렉션을 수행해 테스트를 찾아 해당 테스트를 실행하는 방식은 테스트 러너에서 제어한다.
            - 클래스에 사용되는 러너는 @RunWith 애너테이션으로 설정 할 수 있다.
        6. 햄크레스트 매처와 assertThat()
            - 햄크레스트는 매칭 조건을 선언적으로 작성하는 프레임워크다.
            - 햄크레스트의 매처는 특정 객체가 어떤 조건과 일치하는지 알려주며, 해당 조건이나 객체가 어떤 조건과 일치 하지 않는 이유를 기술 할 수 있다.
            ```
                String s = "yes we have no bananas today";
                
                Matcher<String> containsBananas = new StringContains("bananas");
                Matcher<String> containsMangoes = new StringContains("mangoes");
                
                assertTrue(containsBananas.matches(s));
                assertFalse(containsMangoes.matches(s));
                
                // 대채 매처는 직접적으로 인스턴스화되지 않는다. 코드의 가족성을 높이고자 모든 매처에 대한 정적 팩토리 메서드를 제공한다.
                
                변경!!
                
                assertTrue(containsString("bananas").matches(s));
                assertFalse(containsString("mangoes").matches(s));
                
                // assertThat 과 조합해서 위 코드를 아래와 같이 사용!!!
                
                assertThat(s,containsString("bananas"));
                assertThat(s,not(containsString("mangoes")));
            ``` 
            - 위 코드에서 not() 은 전달된 매처의 의미와 반대 되는 매처를 생성하는 팩터리 함수다.
        7. JMock2: 목객체
            - JMock은 목 객체를 동적으로 생성하므로 목을 생성하려는 타입의 구현제를 직접 작성하지 않아도 된다.
            - JMock API의 핵심 개년은 모조 객체 , 목객체 , 예상 구문이다.
                - 모조 객체 란 : 테스트 대상 객체의 콘텍스트 이며, 해당 객체를 통해 테스트 대상의 행위를 검증 할수 있다.
                - 목 객체 : 실체 SUT 를 대신하는 객체
                - 얘상 구문 : 테스트 과정에서 테스트 대상 객체가 그것의 이웃을 어떻게 호출해야 하는지 기술
                ```
                    @RunWith(JMock.class)
                    public class AuctionMessageTranslatorTest {
                        private final Mockery context = new JUnit4Mockery();
                        private final AuctionEventListener listener = context.mock(AuctionEventListener.class);
                        private final AuctionMessageTranslator translator = new AuctionMessageTranslator(listener);
                        
                        @Test public void notifiesAuctionCloseWhenCloseMessageReceived() {
                            Message message = new Message();
                            message.setBody("SOLVersion : 1.1; Event: Close;");
                            
                            context.checking(new Expectations() {{
                                oneOf(listener).auctionClosed();
                            }});
                            
                            translator.processMessage(UNUSED_CHAT, message);
                        }
                    }
                ```       
                - 위 코드 정리
                    1. 테스트 러너를 사용해 테스트가 끝나는 시점에 모조 객체를 자동으로 호출해 모든 목 객체가 예상대로 호출됐는지 검사
                    2. 관례상 jMock 테스트 에서는 모조 객체를 context 라는 필드에 보관 
                    3. 모조 객체를 사용해 AuctionEventListener 의 목 객체 생성
                    4. 테스트 대상 객체에 목객체 전달 
                    5. 테스트에 사용되는 다른 객체를 준비
                    6. 예상 구문 블록을 정의 
                    7. 테스트 하고자 하는 행위를 일으키는 외부 이벤트 인 테스트 대상 객체를 호출, 만약 예상과 달리 호출되면 즉시 테스트가 실패
                - 예상 구문 를 통해 할수 있는 부분 
                    1. 예상하는 호출의 최소 횟수와 최대 횟수
                    2. 호출이 예상되거나 단순히 호출이 일어나는 것을 허용하는지 여부
                    3. 매개 변수 값, 상수로 만들어 지정하거나 햄크레스트 매처로 제한 
                    4. 다른 예상 구문을 고려한 제약 조건의 순서 지정
                    5. 메서드가 호출 됐을때 일어나야 할 일로 반환할 값이나 던질 예외, 또는 그 밖의 행위
                - 예상 구문 블록은 SUT 가 어떻게 호출 되는지 기술하는 코드와 실제로 객체를 호출하고 결과를 검사하는 코드를 분명하게 구분하는데 목적
                
        
                    
            