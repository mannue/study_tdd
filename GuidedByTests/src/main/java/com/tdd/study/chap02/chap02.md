#study_tdd

- 객체를 활용한 테스트 주도 개발
    1. 객체망
        - 객체 지향 설계는 객체 자체보다 객체 간의 의사소통에 더 집중한다.
        - *시스템은 객체를 생성해 서로 메세지를 주고 받을수 있게 조립하는 과정을 거쳐 만들어지며 시스템의 행위는 객체의 조합(객체의 선택과 연결방식)을 통해 나타나는 특성이다.*
        - 객체 구성을 관리할 목적으로 작성하는 코드를 객체망의 행위에 대한 선언적 정의라 한다.
    
    2. 값과 객체
        - *값은 변하지 않는 양이나 크기를 나타낸다.*
        - *객체는 시간이 지남에 따라 상태가 변할지도 모르지만 식별자가 있는 계산 절차를 나타낸다.*
        - 현장에서는 기능적으로 다루는 값 과 시스템에서 상태를 지닌 행위를 구현하는 객체로 나눈다.
    
    3. 메시지를 따르라
        - 의사소통 패턴은 객체들이 다른 객체와 상호작용하는 방법을 관장하는 각종 규칙으로 구성돼 있다.
        - *자바 같은 언어에서는 구상클래스 대신 추상 인터페이스를 이용해 객체의 역할을 파악한다.*
        - 도메인 모델
            - 특정 문제와 관련된 모든 주제의 개념 모델이다. 도메인 모델은 엔티티 , 엔티티의 속성, 역할 , 관계, 제약을 기술한다.
        - 역할 , 책임 , 협렵자
            - 객체는 역할을 하나 이상 구현한 것이며 역할은 관련된 책임의 집합
            - 책임은 어떤 과업을 수행하거나 정보를 알아야 할 의무를 말한다
            - 협력은 객체나 역할 의 상호 작용에 해당한다.
        ```
            CRC 카드 란
            - 클래스를 정의 하기위한 키워드 산출및 클래스간의 관계를 간단하게 표기하는 문서
            - 출처: https://uiandwe.tistory.com/465 [조아하는모든것]
        ```   
    
    4. 묻지 말고 말하라
        ```
            디미터의 법칙
                - 최소 지식의 원칙 이라고도 하는데 모르는 사람한테 일시키지 말자는 원칙이다.
                - 자신과 직접적인 관련이 있는 클래스에게 일을 시켜야 모듈간의 의존성을 낮추어 진다.
                - 출처: https://supercoding.tistory.com/29 [Super Coding]
        ``` 
        - *객체를 호출할 땐 이웃 객체가 하는 역할 측면에서 해당 객체가 무엇을 원하는지 기술하고 호출된 객체가 그러한 바를 어떻게 실현할지 결정하게 해야한다.*
        - *객체는 내부적으로 보유하고 있거나 메시지를 통해 확보한 정보만 가지고 의사 결정을 내려야 한다.*
        - 이 스타일을 일관되게 따른다면 코드가 좀 유연해지는데, 이는 같은 역할을 수행하는 객체를 손쉽게 교체할 수 있기 때문이다.
        - *'묻지말고 말하라' 원칙 덕분에 접근자 메서드가 연이어 호출 되도록 암시적으로 두기보다는 객체 간의 상호 작용을 명시적으로 만들고 거기에 이름을 부여하게 된다.*
        ```
            ((EditSaveCustomizer) master.getModelisable().getDockablePanel().getCustomizer())
                            .getSaveItem().setEnabled(Boolean.FALSE.booleanValue());
                            
            // 위 코드를 아래와 같이 변경
            master.allowSavingOfCustomisations();
        ```
    
    5. 그래도 가끔은 물어라
        - 값과 컬렉션으로 부터 정보를 가져오거나 팩터리를 이용해 새 객체를 생성할 때는 '묻는다'
        ```
            public class Train {
                private final List<Carriage> carriages [...]
                private int percentReservedBarrier = 70;
                
                public void reserveSeats(ReservationRequest request) {
                    for (Carriage carriage : cattiages) {
                        if (carriage.getSeats().getPercentReserved() < percentReservedBarrier) {
                            request.reserveSeatsIn(carriage);
                            return;
                        }
                    }
                    request.cannotFindSeats();
                }
            }

            public void reserveSeats(ReservationRequest request) {
                for (Carriage carriage : cattiages) {
                    if (carriage.hasSeatsAvailableWithin(percentReservedBarrier)) {
                        request.reserveSeatsIn(carriage);
                        return;
                    }
                }
                request.cannotFindSeats();
            }
        ``` 
        - 이를 구현한답시고 Carriage 의 내부 구조를 노출해서는 안되는데, 이는 기차안에 다양한 유형의 객차가 있기 때문만은 아니다.
        - *스스로 답을 가늠하는데 도움이 되는 정보를 묻기보다는 진정 답하고자 하는 질문을 던져야 한다.*
        - *질의 메서드를 추가하면 가장 적절한 객체에 행위가 자리 잡아 행위에 이해하기 쉬운 이름이 생기고 테스트하기가 쉬워진다*
        - 호출하는 객체의 의도를 서술하는 질의를 작성하려고 애써야한다.
        
    6. 협력 객체의 단위 테스트
        - 테스트에 존재하는 대상 객체의 이웃을 다른 대체물, 즉 목 객체로 대처한다.
        - 목객체로 대처하면 발생하는 이벤트에 대해 대상 객체가 가짜 이웃과 어떻게 상호 작용할지 지정할수 있으며 이 같은 명세를 예상 구문 이라 한다.
        - 실제로 단위 테스트를 작성할때는 그러한 협력자가 있을 필요가 없다. 이러한 보조 역할은 자바 인터페이스로 정의돼 있고 시스템의 나머지 부분을 개발 할때 실제 구현처럼 동작한다
        ```
            TestObject mock = mock(TestObject.class);
            when(mock.getId()).thenReturn("value");
    
            TestWork test = new TestWork(mock);
    
            String id = test.getId();
    
            assertThat(id,is("value"));
        ```
        
    7. 목 객체를 활용한 TDD 지원 
        - 테스트 주도 프로그래밍을 지원하는 순서
            1. 이웃하는 객체의 목 인스턴스를 생성
            2. 목 인스턴스를 어떻게 호출하고 상태를 검사하는가에 관한 예상 구문을 정의
            3. 테스트를 할때 스텁 형태로 동작할 필요하 있는 행위를 구현
        - 단위 테스트는 대상 객체와 해당 객체를 둘러싼 환경 간의 관계를 명확하게 드러낸다.
        - 단위 테스트는 그곳에 있는 모든 객체를 생성하고 대상 객체와 해당 객체의 협력자 사이의 상호 작용에 관한 단정을 만들어 낸다.
           
              